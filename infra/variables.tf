# Declaração de nomes de variaveis para utilizar na main de cada setor
variable "região_aws" {
    type = string
}
variable "chave" {
    type = string
}
variable "instancia" {
    type = string
}
variable "ami" {
    type = string
}
variable "iac_name" {
    type = string
}